"use strict";

const Store = require("orbit-db-store")
const CommunityIndex = require("./CommunityIndex")

class CommunityStore extends Store {
  constructor(ipfs, id, dbname, options = {}) {
    if(options.Index === undefined) Object.assign(options, { Index: CommunityIndex })
    super(ipfs, id, dbname, options)
    this._type = "communitystore"
    this.events.on("log.op.ADD", (address, hash, payload) => {
      this.events.emit("db.append", payload.value)
    })
  }

  add(data, options = {}) {
    return this._addOperation({
      op: "ADD",
      key: null,
      value: data
    }, options)
  }
}
