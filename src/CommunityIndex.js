"use strict"


class CommunityIndex {
  /**
   * Parse the oplog and create a state of the Community.
   * Content is stored by referenc using CIDs! They have to pinned separately.
   */
  constructor() {
    this._index = CommunityIndex.emptyIndex
  }

  /**
   * Operations:
   * 1. Add Content (No content, only cid is stored.)
   * 2. Remove Content (Only CID)
   * 3. Start an Election
   * 4. Propose to an Election
   * 5. Vote on an Election
   * 6. Certify an Election
   * 7. Request Ruling
   * 8. Make Ruling
   * 9. Apply for membership
   * 10. Accept membership
   */
  static operations = {
    ADDCONTENT: 1,
    REMOVECONTENT: 2,
    STARTELECTION: 3,
    PROPOSE: 4
    VOTE: 5,
    CERTIFY: 6,
    REQUESTRULING: 7,
    MAKERULING: 8,
    APPLYFORMEMBERSHIP: 9,
    ACCEPTMEMBERSHIP: 10 
  }

  static emptyIndex = { offices: { president: null },
                        content: [],
                        elections: [],
                        resolutions: [],
                        rulings: [],
                        membership: {
                          applications: {},
                          accepted: {}
                        },
                        services: {}
                      }

  // OFFICES
  /**
   * Get all office holders.
   * @param title string title of the office
   * @returns {Array<CID>|CID} if title == "president", only a single element.
   */
  getOffice(title) {
    return this._index.offices[title]
  }

  // CONTENT

  // ELECTIONS

  // RESOLUTIONS

  // RULINGS

  // MEMBERSHIP

  // SERVICES
  /**
   * Get a service CID
   */
  getService(id) {
    return this._index.services[id]
  }

  /**
   * Put in a service and cid
   */
  _putService(service, cid) {
    this._index.services[service] = cid
  }

  updateIndex(oplog) {
    this._index = CommunityIndex.emptyIndex
    oplog.values.reduce((handled, item) => {
      if(!handled.includes(item.hash)) {
        handled.push(item.hash)

        switch (item.payload.op) {
          case this.ops:

            break;
          default:

        }

      }
    })
  }
}

try {
  module.exports = CommunityIndex
} catch (e) {
  window.CommunityIndex = CommunityIndex
}
